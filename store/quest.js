import _ from 'lodash'
import VuexPersistence from 'vuex-persist'

export const plugins = [VuexPersistence]

export const state = () => ({
  questlines: [],
  quests: [],
  isLoggedIn: false,
})

export const mutations = {
  addQuestline(state, questline) {
    const questlines = state.questlines

    const res = _.find(questlines, { id: questline.id })

    if (!res) {
      questlines.push(questline)
    }
  },
  addQuest(state, quest) {
    const quests = state.quests

    const res = _.find(quests, { id: quest.id })

    if (!res) {
      quests.push(quest)
    }
  },
  changeIsLoggedIn(state, status) {
    state.isLoggedIn = status
  },
  removeAll(state) {
    state.questlines = []
    state.quests = []
    state.isLoggedIn = false
  },
}
