import _ from 'lodash'

export default function ({ $axios, store }, inject) {
  const opentour = {
    sync() {
      return $axios
        .patch(`/api/sync_quests`, {
          finished_quests: _.map(store.state.quest.quests, 'id'),
        })
        .then((res) => res.data)
        .then((data) => {
          _.each(
            data.finished_quests,
            store.commit.bind(null, 'quest/addQuest')
          )

          _.each(
            data.finished_questlines,
            store.commit.bind(null, 'quest/addQuestline')
          )
        })
    },
  }

  inject('opentour', opentour)
}
