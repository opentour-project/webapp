FROM node:lts AS webapp

EXPOSE 3000

ENV APP_ROOT=/opt/app \
    APP_USER=app

WORKDIR $APP_ROOT

RUN useradd -d $APP_ROOT -r $APP_USER && \
    chown $APP_USER:$APP_USER $APP_ROOT

USER $APP_USER

COPY package.json $APP_ROOT
COPY package-lock.json $APP_ROOT

RUN npm ci

COPY . $APP_ROOT

RUN npm run generate

FROM nginx:1-alpine

LABEL name=opentour-webapp \
      version=0.1.0 \
      maintainer="kaisar@arkhan.io"

ENV APP_ROOT=/opt/app

COPY --from=webapp $APP_ROOT/dist $APP_ROOT/dist

COPY docker/default.conf /etc/nginx/conf.d/default.conf
